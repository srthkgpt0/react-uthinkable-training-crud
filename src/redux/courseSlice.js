import { createSlice } from '@reduxjs/toolkit'
export const courseSlice = createSlice({
  name: 'course',
  initialState: [
    {
      id: 0,
      title: 'Building Application in React and Flux',
      length: '5:08',
      category: 'JavaScript',
      author: 'cory house',
      link: 'react-flux-building-application'
    },
    {
      id: 1,
      title: 'Clean code: Writing Code For Humans',
      length: '3:10',
      category: 'Software Practices',
      author: 'cory house',
      link: 'writing-clean-code-humans'
    },
    {
      id: 2,
      title: 'Architecting Application for Real world',
      length: '2:52',
      category: 'Software Architecture',
      author: 'cory house',
      link: 'architecting-applications-dotnet'
    },
    {
      id: 3,
      title: 'Becoming an Outlier: Reprogramming the Development',
      length: '2:30',
      category: 'Career',
      author: 'cory house',
      link: 'career-reboot-for-developer-mind'
    },
    {
      id: 4,
      title: 'Web component fundamentals',
      length: '5:10',
      category: 'HTML5',
      author: 'cory house',
      link: 'web-component-shadow-dom'
    }
  ],
  reducers: {
    ADD: (state, action) => {
      let [temp] = state.slice(-1)
      let newId = temp.id + 1
      const obj = [
        {
          ...action.payload,
          id: newId,
          link: action.payload.title
        }
      ]
      return [...state, ...obj]
    },
    EDIT: (state, action) => {
      const courses = state.filter((a) => a.id !== action.payload.id)
      const course = [{ ...action.payload, link: action.payload.title }]
      return [...courses, ...course]
    },
    DELETE: (state, action) => {
      const courses = state.filter((a) => a.id !== action.payload)
      return [...courses]
    }
  }
})
export const { ADD, EDIT, DELETE } = courseSlice.actions
export default courseSlice.reducer
