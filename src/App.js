import React from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import Header from './component/Header'
import Routes from './routes'
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css'
function App() {
  return (
    <Router>
      <Header />
      <Routes />
    </Router>
  )
}

export default App
