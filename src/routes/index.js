import React from 'react'
import { Route, Switch } from 'react-router-dom'
import About from '../container/About'
import AddEdit from '../container/Add'
import Courses from '../container/Courses'
import Home from '../container/Home'
import 'antd/dist/antd.css'
function Routes() {
  return (
    <Switch>
      <Route exact path='/' component={Home} />
      <Route exact path='/courses' component={Courses} />
      <Route exact path='/about' component={About} />
      <Route exact path='/course' component={AddEdit} />
    </Switch>
  )
}

export default Routes
