import { configureStore } from '@reduxjs/toolkit'
import courseReducer from '../redux/courseSlice'

export default configureStore({
  reducer: {
    course: courseReducer
  }
})
