/* eslint-disable no-undef */
import React from 'react'

function Home() {
  
 
  return (
    <div>
      <header>
        <div className='jumbotron jumbotron-fluid bg-info text-white text-center'>
          <div className='container'>
            <h1 className='display-4 text-white'>ReactJS CRUD Boilerplate</h1>
            <p className='lead'>with mock backend</p>
          </div>
        </div>
      </header>
      <div className='container text-muted'>
        <section className='row'>
          <div className='col-md-6 col-lg-3'>
            <div className='card'>
              <a href='https://reactjs.org/'>
                <img
                  className='card-img-top'
                  src={process.env.PUBLIC_URL + '/reactjs.c1795ade.jpg'}
                  alt='Card1'
                />
              </a>
              <div className='card-body'>
                <h3 className='card-title'>ReactJS</h3>
                <p className='card-text'>ReactJS/Redux using the ES6 syntax</p>
              </div>
            </div>
          </div>
          <div className='col-md-6 col-lg-3'>
            <div className='card'>
              <a href='https://reactjs.org/'>
                <img
                  className='card-img-top'
                  src={process.env.PUBLIC_URL + '/createReactApp.jpg'}
                  alt='Card2'
                />
              </a>
              <div className='card-body'>
                <h3 className='card-title'>create-react-app</h3>
                <p className='card-text'>
                  Official. No Setup. Built in HMR, ESLint, Jest, Coverage
                </p>
              </div>
            </div>
          </div>
          <div className='col-md-6 col-lg-3'>
            <div className='card'>
              <a href='https://reactjs.org/'>
                <img
                  className='card-img-top'
                  src={process.env.PUBLIC_URL + '/reactrouter.png'}
                  alt='Card3'
                />
              </a>
              <div className='card-body'>
                <h3 className='cart-title'>React Router 4</h3>
                <p className='card-text'>
                  Declarative routing for ReactJS apps.
                </p>
              </div>
            </div>
          </div>
          <div className='col-md-6 col-lg-3'>
            <div className='card'>
              <a href='https://reactjs.org/'>
                <img
                  className='card-img-top'
                  src={process.env.PUBLIC_URL + '/bootstrap.jpg'}
                  alt='Card4'
                />
              </a>
              <div className='card-body'>
                <h3 className='card-title'>Bootstrap 4</h3>
                <p className='card-text'>
                  A framework for styling apps for all screen sizes.
                </p>
              </div>
            </div>
          </div>
        </section>
        <footer>
          <h2 className='display-4 text-center py-4 my-4'>Features</h2>
          <nav className='nav nav-pills justify-content-center'>
            <ul className='nav nav-pills mb-3' id='ex1' role='tablist'>
              <li className='nav-item' role='presentation'>
                <a
                  className='nav-link active'
                  id='ex1-tab-1'
                  data-mdb-toggle='pill'
                  href='#ex1-pills-1'
                  data-toggle='tab'
                  role='tab'
                  aria-controls='ex1-pills-1'
                  aria-selected='true'
                >
                  Mock Rest
                </a>
              </li>
              <li className='nav-item' role='presentation'>
                <a
                  className='nav-link'
                  id='ex1-tab-2'
                  data-mdb-toggle='pill'
                  href='#ex1-pills-2'
                  data-toggle='tab'
                  role='tab'
                  aria-controls='ex1-pills-2'
                  aria-selected='false'
                >
                  Unit Tests
                </a>
              </li>
              <li className='nav-item' role='presentation'>
                <a
                  className='nav-link'
                  id='ex1-tab-3'
                  data-mdb-toggle='pill'
                  href='#ex1-pills-3'
                  data-toggle='tab'
                  role='tab'
                  aria-controls='ex1-pills-3'
                  aria-selected='false'
                >
                  ES2015
                </a>
              </li>
              <li className='nav-item' role='presentation'>
                <a
                  className='nav-link'
                  id='ex1-tab-4'
                  data-mdb-toggle='pill'
                  href='#ex1-pills-4'
                  data-toggle='tab'
                  role='tab'
                  aria-controls='ex1-pills-4'
                  aria-selected='false'
                >
                  Others
                </a>
              </li>
            </ul>
          </nav>

          <div className='tab-content py-5' id='ex1-content'>
            <div
              className='tab-pane fade show active'
              id='ex1-pills-1'
              role='tabpanel'
              aria-labelledby='ex1-tab-1'
            >
              <h3>Mock Rest</h3>
              <p>Custom written mock REST</p>
            </div>
            <div
              className='tab-pane fade'
              id='ex1-pills-2'
              role='tabpanel'
              aria-labelledby='ex1-tab-2'
            >
              <h3>Unit Testing</h3>
              <ul>
                <li>Jest</li>
                <li>Enzyme</li>
                <li>Nock</li>
                <li>Expect assertion</li>
                <li>Code Coverage</li>
              </ul>
            </div>
            <div
              className='tab-pane fade'
              id='ex1-pills-3'
              role='tabpanel'
              aria-labelledby='ex1-tab-3'
            >
              <h3>ES2015</h3>
              <p>aka es6</p>
            </div>
            <div
              className='tab-pane fade'
              id='ex1-pills-4'
              role='tabpanel'
              aria-labelledby='ex1-tab-4'
            >
              <h3>Others</h3>
              <ul>
                <li>Redux</li>
                <li>Redux Form</li>
                <li>Lodash</li>
                <li>React Bootstrap table</li>
                <li>Font Awesome (For icons)</li>
                <li>Hot Module Replacement (HMR)</li>
              </ul>
            </div>
          </div>
        </footer>
      </div>
    </div>
  )
}

export default Home
