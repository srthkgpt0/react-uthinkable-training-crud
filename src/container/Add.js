import React, { useEffect, useState } from 'react'
import { Form, Input, Select } from 'antd'
import { useHistory } from 'react-router-dom'
import { ADD, EDIT } from '../redux/courseSlice'
import { useDispatch, useSelector } from 'react-redux'
function AddEdit() {
  const history = useHistory()
  const [id] = useState(history.location.state)
  const [form] = Form.useForm()

  const courses = useSelector((state) => state.course)
  useEffect(() => {
    const [course] = courses.filter((course) => course.id === id)
    form.setFieldsValue(course)
  }, [id])
  const dispatch = useDispatch()

  const author = [
    {
      name: 'Cory House',
      value: 'cory house'
    },
    {
      name: 'Scott Allen',
      value: 'scott allen'
    },
    {
      name: 'Dan Wahlin',
      value: 'dan wahlin'
    }
  ]
  const onFinish = (values) => {
    if (id) {
      dispatch(EDIT({ ...values, id }))
    } else {
      dispatch(ADD(values))
    }
    history.push('/courses')
  }

  return (
    <div className='container'>
      <h1>Add</h1>
      <Form form={form} onFinish={onFinish}>
        <div className='form-group'>
          <label>Title</label>
          <Form.Item
            name='title'
            rules={[{ required: true, message: 'Required' }]}
          >
            <Input placeholder='Title of the course' />
          </Form.Item>
        </div>
        <div className='form-group'>
          <label>Author</label>
          <Form.Item
            name='author'
            rules={[{ required: true, message: 'Required' }]}
          >
            <Select>
              {author.map((a, index) => (
                <Select.Option key={index} value={a.value}>
                  {a.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </div>
        <div className='form-group'>
          <label>Category</label>
          <Form.Item
            name='category'
            rules={[{ required: true, message: 'Required' }]}
          >
            <Input placeholder='Category of the course' />
          </Form.Item>
        </div>
        <div className='form-group'>
          <label>Length</label>
          <Form.Item
            name='length'
            rules={[{ required: true, message: 'Required' }]}
          >
            <Input placeholder='Length of the course in minutes or hours' />
          </Form.Item>
        </div>
        <Form.Item>
          <div className='row'>
            <div className='btn-group'>
              <button type='submit' className='btn btn-primary m-1'>
                Submit
              </button>
              {!id && (
                <button type='reset' className='btn btn-light m-1'>
                  Clear Values
                </button>
              )}
              <button
                onClick={() => history.push('/courses')}
                className='btn btn-light m-1'
              >
                Cancel
              </button>
            </div>
          </div>
        </Form.Item>
      </Form>
    </div>
  )
}

export default AddEdit
