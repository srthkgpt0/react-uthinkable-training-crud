import React, { useState } from 'react'
import { Link, useHistory } from 'react-router-dom'
import BootstrapTable from 'react-bootstrap-table-next'
import { useDispatch, useSelector } from 'react-redux'
import { DELETE } from '../redux/courseSlice'

function Courses() {
  const [isSelect, setIsSelect] = useState(false)
  const [row, setRow] = useState({})
  const dispatch = useDispatch()
  const history = useHistory()
  const products = useSelector((state) => state.course)
  const columns = [
    {
      dataField: 'id',
      text: 'id',
      sort: true,
      hidden: true
    },
    {
      dataField: 'title',
      text: 'Title',
      sort: true,
      // eslint-disable-next-line react/display-name
      formatter: (cell, row) => (
        <a href={`https://www.pluralsight.com/courses/${row.link}`}>{cell}</a>
      )
    },
    {
      dataField: 'length',
      text: 'Length',
      sort: true
    },
    {
      dataField: 'category',
      text: 'Category',
      sort: true
    },
    {
      dataField: 'author',
      text: 'Author',
      sort: true
    }
  ]

  const selectRow = {
    mode: 'radio',
    hideSelectColumn: true,
    clickToSelect: true,
    style: { backgroundColor: 'rgb(193, 242, 145)' },
    hideSelectAll: true,
    onSelect: (row, isSelect) => {
      setRow(row)
      setIsSelect(isSelect)
    }
  }
  const rowStyle = (row, rowIndex) => {
    const style = {}
    if (rowIndex % 2 === 0) {
      style.backgroundColor = 'rgba(54, 163, 173, .10)'
    } else {
      style.backgroundColor = 'transparent'
    }
    style.borderTop = 'none'

    return style
  }
  const handleEdit = () => {
    history.push('/course', row.id)
  }
  const handleDelete = () => {
    dispatch(DELETE(row.id))
  }
  return (
    <div className='container-fluid'>
      <div className='row mt-3'>
        <div className='col'>
          <h1>Courses</h1>
        </div>
      </div>
      <div className='btn-group mt-3'>
        <Link to='/course' type='button' className='btn btn-primary m-1'>
          <i className='fas fa-plus m-1'></i>
          New
        </Link>
        <button
          type='button'
          className='btn btn-warning text-white m-1'
          disabled={!isSelect}
          onClick={() => handleEdit()}
        >
          <i className='fas fa-pen  m-1'></i>
          Edit
        </button>
        <button
          type='button'
          className='btn btn-danger m-1'
          disabled={!isSelect}
          onClick={() => handleDelete()}
        >
          <i className='fas fa-trash-alt m-1'></i>
          Delete
        </button>
      </div>
      {products && (
        <BootstrapTable
          keyField='id'
          data={products}
          columns={columns}
          selectRow={selectRow}
          rowStyle={rowStyle}
        />
      )}
    </div>
  )
}

export default Courses
